from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants


class MyPage(Page):
    form_model = 'player'
    form_fields = ['active']


class ResultsWaitPage(WaitPage):
    def is_displayed(self):
        return self.player.active

    def after_all_players_arrive(self):
        self.group.group_results()


class Results(Page):
    def is_displayed(self):
        return self.player.active


page_sequence = [MyPage, ResultsWaitPage, Results]
