# Possible bug in after_all_players_arrive()

I have problem where the function `after_all_players_arrive()` is not run on a Waitpage when a player of the group does not arrive at the Waitpage. The occurrence of the problem depends on the order in which the players go through the game.

Say we have an auction with 2 players. Bidders submit their bids on one page and go to a waitpage afterwards. After all players have arrived at the waitpage, `after_all_players_arrive()` is run and determines the winning bidder etc.

Assume that there is the possibility to drop out of the auction. In this case, the player who dropped out does not go to the waitpage but arrives at the end of the experiment. In this case, I would assume that the code in `after_all_players_arrive()` on the waitpage is also run if at least one bidder is waiting.

However, if the player to drop out is the last one to enter their decision, the code in `after_all_players_arrive()` is not run. If a still active bidder enters their decision last, the code in `after_all_players_arrive()` is run as expected.

## Steps to reproduce the bug

- Open both participants
- Input 'Yes' in some player
- Input 'No' in the other player

## Expected output

- The Waitpage `ResultsWaitPage` should run the code defined in `after_all_players_arrive()` and set the group variable `output` to True.  

## Actual output

- The code in `after_all_players_arrive()` is never run and hence, `group.output` is still at its initial value of `False`.

## Case where the problem does not occur

If you change the order of the input and first set one player 'No' and only then set the other player to yes, the code in `after_all_players_arrive()` is run as expected.

- Open both participants
- Input 'No' in some player
- Input 'Yes' in the other player


